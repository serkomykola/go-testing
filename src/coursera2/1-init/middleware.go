package main

import (
	"fmt"
	"net/http"
	"time"
)

const (
	SESSION_COOKIE = "session_id"
)

// ---- Pages ----
func mainPage(w http.ResponseWriter, r *http.Request) {
	session, err := r.Cookie(SESSION_COOKIE)
	loggedIn := (err != http.ErrNoCookie)

	if loggedIn {
		fmt.Fprintln(w, `<a href="/logout">logout</a>`)
		fmt.Fprintln(w, "Welcome "+session.Value)
	} else {
		fmt.Fprintln(w, `<a href="/login">login</a>`)
		fmt.Fprintln(w, "You need to login")
	}
}

func loginPage(w http.ResponseWriter, r *http.Request) {
	fmt.Println("login page")

	expiration := time.Now().Add(10 * time.Minute)
	cookie := &http.Cookie{
		Name:    SESSION_COOKIE,
		Value:   "serko",
		Expires: expiration,
	}

	http.SetCookie(w, cookie)
	http.Redirect(w, r, "/", http.StatusFound)
}

func logoutPage(w http.ResponseWriter, r *http.Request) {
	fmt.Println("logout page")

	session, err := r.Cookie(SESSION_COOKIE)
	if err == http.ErrNoCookie {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	session.Expires = time.Now().AddDate(0, 0, -1)

	http.SetCookie(w, session)
	http.Redirect(w, r, "/", http.StatusFound)
}

func adminIndex(w http.ResponseWriter, r *http.Request) {
	fmt.Println("admin index page")

	fmt.Fprintln(w, `<a href="/">site index</a>`)
	fmt.Fprintln(w, "Admin main page")
}
func panicPage(w http.ResponseWriter, r *http.Request) {
	fmt.Println("panic page")
	panic("this must be recover")
}

// ---- Middlewares ---
func adminAccessMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Admin Auth Middleware", r.URL.Path)

		_, err := r.Cookie(SESSION_COOKIE)
		if err != nil {
			fmt.Println("no auth at", r.URL.Path)
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func accessLogMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Access Log middleware", r.URL.Path)
		start := time.Now()
		next.ServeHTTP(w, r)
		fmt.Printf("[%s] %s, %s %s \n",
			r.Method, r.RemoteAddr, r.URL.Path, time.Since(start))
	})
}

func panicMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Panic middleware", r.URL.Path)
		defer func() {
			if err := recover(); err != nil {
				fmt.Println("recover", err)
				http.Error(w, "Internal server error", http.StatusInternalServerError)
			}
		}()

		next.ServeHTTP(w, r)
	})
}

func main() {
	fmt.Println("http middleware")

	adminMux := http.NewServeMux()
	adminMux.HandleFunc("/admin/", adminIndex)
	adminMux.HandleFunc("/admin/panic", panicPage)

	// set access middleware
	adminHandler := adminAccessMiddleware(adminMux)

	siteMux := http.NewServeMux()
	siteMux.Handle("/admin/", adminHandler)
	siteMux.HandleFunc("/login", loginPage)
	siteMux.HandleFunc("/logout", logoutPage)
	siteMux.HandleFunc("/", mainPage)

	// set panic andaccess log middleware
	siteHandler := accessLogMiddleware(siteMux)
	siteHandler = panicMiddleware(siteHandler)

	fmt.Println("starting server at :8081")
	fmt.Println(http.ListenAndServe(":8081", siteHandler))
}
