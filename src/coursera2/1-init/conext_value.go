package main

import (
	"bytes"
	"context"
	"fmt"
	"math/rand"
	"net/http"
	"sync"
	"time"
)

type key int

const timingsKey key = 1

// Avg sleaping
const AvgSleep = 50

type ctxTimings struct {
	sync.Mutex
	Data map[string]*Timing
}

// Coment
type Timing struct {
	Count    int
	Duration time.Duration
}

func trackContextTimings(ctx context.Context, metricName string, start time.Time) {
	timings, ok := ctx.Value(timingsKey).(*ctxTimings)
	if !ok {
		return
	}

	elapsed := time.Since(start)

	timings.Lock()
	defer timings.Unlock()

	if metric, metricExists := timings.Data[metricName]; !metricExists {
		timings.Data[metricName] = &Timing{
			Count:    1,
			Duration: elapsed,
		}
	} else {
		metric.Count++
		metric.Duration += elapsed
	}
}

func loadPosts(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	emulateWork(ctx, "checkCache")
	emulateWork(ctx, "loadPosts")
	emulateWork(ctx, "loadPosts")
	emulateWork(ctx, "loadPosts")
	time.Sleep(10 * time.Millisecond)

	emulateWork(ctx, "loadSidebar")
	emulateWork(ctx, "loadComments")

	fmt.Fprintln(w, "Site handler")

	fmt.Println("Request done")
}

func emulateWork(ctx context.Context, workName string) {
	defer trackContextTimings(ctx, workName, time.Now())

	rnd := time.Duration(rand.Intn(AvgSleep))
	time.Sleep(rnd * time.Millisecond)
}

func logContextTimings(ctx context.Context, path string, start time.Time) {
	timings, ok := ctx.Value(timingsKey).(*ctxTimings)
	if !ok {
		return
	}

	totalReal := time.Since(start)
	buf := bytes.NewBufferString(path)
	var total time.Duration
	for timing, value := range timings.Data {
		total += value.Duration
		buf.WriteString(fmt.Sprintf("\n\t%s(%d): %s", timing, value.Count, value.Duration))
	}

	buf.WriteString(fmt.Sprintf("\n\tTotal: %s", totalReal))
	buf.WriteString(fmt.Sprintf("\n\tTracked: %s", total))
	buf.WriteString(fmt.Sprintf("\n\tunknown: %s", totalReal-total))

	fmt.Println(buf.String())
}

func timingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(),
			timingsKey,
			&ctxTimings{
				Data: make(map[string]*Timing),
			})

		defer logContextTimings(ctx, r.URL.Path, time.Now())
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func main() {
	fmt.Println("Contect value")

	rand.Seed(time.Now().UTC().UnixNano())

	siteMux := http.NewServeMux()
	siteMux.HandleFunc("/", loadPosts)

	siteHandler := timingMiddleware(siteMux)

	fmt.Println("server starting at :8081")
	fmt.Println(http.ListenAndServe(":8081", siteHandler))
}
