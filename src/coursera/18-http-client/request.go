package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"strconv"
	"sync"
	"time"
)

func runGet(urlStr string, wg *sync.WaitGroup) {
	defer wg.Done()

	response, err := http.Get(urlStr)
	if err != nil {
		fmt.Println("Error happens", err)
		return
	}

	// important
	defer response.Body.Close()

	respBody, err := ioutil.ReadAll(response.Body)

	fmt.Println("------------------------------")
	fmt.Printf("http.Get headers \n\t %#v \n", response.Header)
	fmt.Printf("http.Get received \n\t %#v \n", string(respBody))
	fmt.Println("------------------------------")

	contentType := response.Header.Get("Content-Type")
	if contentType == "application/json" && err == nil {
		fmt.Println("There is json")
	}
}

func runGetFullReq(urlStr string, wg *sync.WaitGroup) {
	defer wg.Done()

	req := &http.Request{
		Method: http.MethodGet,
		Header: http.Header{
			"User-Agent": {"serko/agent"},
		},
	}

	req.URL, _ = url.Parse(urlStr)
	req.URL.Query().Set("my-name", "mykola")

	response, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println("Error happens", err)
		return
	}

	// important
	defer response.Body.Close()

	respBody, err := ioutil.ReadAll(response.Body)

	fmt.Println("------------------------------")
	fmt.Printf("runGetFullReq headers \n\t %#v \n", response.Header)
	fmt.Printf("runGetFullReq received \n\t %#v \n", string(respBody))
	fmt.Println("------------------------------")
}

func runTransportAndPost(urlStr string, wg *sync.WaitGroup) {
	defer wg.Done()

	transport := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
			DualStack: true,
		}).DialContext,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
	}

	client := &http.Client{
		Timeout:   10 * time.Second,
		Transport: transport,
	}

	data := `{"name" "serko", "age": 28}`
	body := bytes.NewBufferString(data)

	req, _ := http.NewRequest(http.MethodPost, urlStr, body)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Content-Length", strconv.Itoa(len(data)))
	req.Header.Add("User-Agent", "Serko/agent-transport")

	response, err := client.Do(req)
	if err != nil {
		fmt.Println("Error happens", err)
		return
	}

	// important
	defer response.Body.Close()

	respBody, err := ioutil.ReadAll(response.Body)

	fmt.Println("------------------------------")
	fmt.Printf("runTransportAndPost headers \n\t %#v \n", response.Header)
	fmt.Printf("runTransportAndPost received \n\t %#v \n", string(respBody))
	fmt.Println("------------------------------")
}

func main() {
	fmt.Println("Http client simple")

	urlGet := "https://httpbin.org/get"

	wg := &sync.WaitGroup{}

	wg.Add(1)
	go runGet(urlGet, wg)

	wg.Add(1)
	go runGetFullReq(urlGet, wg)

	wg.Add(1)
	urlPost := "https://httpbin.org/post"
	go runTransportAndPost(urlPost, wg)

	wg.Wait()

}
