package main

import (
	"fmt"
)

const (
	hello = `Str const`
	pi    = 3.14
)

// iota
const (
	zero = iota
	_
	three
)

// typed constants
const (
	year         = 2017
	yerTyped int = 2017
)

func main() {
	fmt.Println("hello const", hello, pi, zero, three)

	var month uint32 = 34
	fmt.Println(`Typed const`, month+year)

	// ponters
	a := 2
	b := &a

	*b = 3
	fmt.Println(`pointers`, a, *b, b)

	c := &a
	// new pointer for unnamed variable with default value
	d := new(int)
	*d = 12
	fmt.Println(`next pointers`, *c, *d, c, d)

	*c = *d
	*d = 13

	c = d
	fmt.Println(`next pointers`, a, *c, *d, c, d)
}
