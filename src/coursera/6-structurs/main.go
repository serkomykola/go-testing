package main

import (
	"fmt"
)

type Person struct {
	Name    string
	City    string
	Address string
}

type Account struct {
	Id      int
	Name    string
	Cleaner func(string) string
	Person
}

type MySlice []int

// methods
func (p Person) UpdateNameWrong(name string) {
	p.Name = name
}

func (p *Person) UpdateName(name string) {
	p.Name = name
}

func (sl *MySlice) AddInt(num int) {
	*sl = append(*sl, num)
}

func (sl *MySlice) Count() int {
	return len(*sl)
}

func main() {
	fmt.Println("Structures")

	// full declaration. Something fields may be empty (define with default values)
	var acc Account = Account{
		Id:   23,
		Name: "test name",
		Person: Person{
			Name: "Second name",
			City: "Kiev",
		},
	}

	fmt.Printf("Account: %#v \n", acc)
	fmt.Println("account name", acc.Name)
	fmt.Println("account person", acc.City, acc.Person.Name)

	// methods
	acc.Person.UpdateNameWrong("WR name")
	fmt.Println("account name 1", acc.Person.Name)
	acc.Person.UpdateName("Correct name")
	fmt.Println("account name 2", acc.Person.Name)
	acc.UpdateName("Correct 2 name")
	fmt.Println("account name 3", acc.Person.Name)

	// slice method
	// slice := MySlice{1, 2, 3}
	slice := MySlice([]int{1, 2, 3})
	slice.AddInt(56)
	fmt.Println("Result slice:", slice, slice.Count())
}
