package main

import (
	"fmt"
)

type Payer interface {
	Pay(int) error
}

type Wallet struct {
	Cash int
}

func (w *Wallet) Pay(amount int) error {
	if w.Cash < amount {
		return fmt.Errorf("Не хватает денег в кошельке!")
	}

	w.Cash -= amount
	return nil
}

type ApplePay struct {
	Balance int
	AppleID string
}

func (a *ApplePay) Pay(amount int) error {
	if a.Balance < amount {
		return fmt.Errorf("Не хватает денег в кошельке!")
	}

	a.Balance -= amount
	return nil
}

func Buy(p Payer) {
	err := p.Pay(10)

	if err != nil {
		panic(err)
	}

	fmt.Printf("paid %T\n", p)
}

func BuyWithEmptyInterface(in interface{}) {
	var p Payer
	var ok bool
	if p, ok = in.(Payer); !ok {
		fmt.Printf("%T not Payer interface \n", in)
		return
	}

	err := p.Pay(7)

	if err != nil {
		panic(err)
	}

	fmt.Printf("paid empty interface %T\n", p)

}

func main() {
	fmt.Println("Interface")

	myWallet := &Wallet{
		Cash: 20,
	}

	Buy(myWallet)

	// Apple pay (define interface)
	var applePay Payer
	applePay = &ApplePay{Balance: 30, AppleID: "nik.ser"}
	Buy(applePay)
	BuyWithEmptyInterface(applePay)
}
