package main

import (
	"context"
	"fmt"
	"math/rand"
	"time"
)

func worker(ctx context.Context, mark int, result chan int) {
	waitTime := time.Duration(rand.Intn(100)+10) * time.Millisecond

	fmt.Println("Worker Num", mark, "sleep", waitTime)
	select {
	case <-ctx.Done():
		return
	case <-time.After(waitTime):
		fmt.Println("worker", mark, "done")
		result <- mark
	}
}

func main() {
	fmt.Println("Go Context")

	ctx, finish := context.WithCancel(context.Background())
	result := make(chan int, 1)

	for i := 0; i < 6; i++ {
		go worker(ctx, i, result)
	}

	foundBy := <-result
	fmt.Println("Received from", foundBy)
	finish()

	// defined work time
	workTime := 50 * time.Millisecond
	ctx2, _ := context.WithTimeout(context.Background(), workTime)
	res2 := make(chan int, 1)

	for i := 0; i < 10; i++ {
		go worker(ctx2, i, res2)
	}

	totalFound := 0
LOOP:
	for {
		select {
		case <-ctx2.Done():
			break LOOP
		case found2 := <-res2:
			totalFound++
			fmt.Println("Received from", found2)
		}
	}

	fmt.Println("Total found", totalFound)

	time.Sleep(1 * time.Second)

}
