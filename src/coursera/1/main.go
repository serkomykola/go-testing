package main

import (
	"fmt"
	"unicode/utf8"
)

func main() {
	fmt.Println("Hello Serko")

	// int
	var int0 int
	var int1 int = 1
	var int2 = 20

	fmt.Println(int0, int1, int2)

	var unsignedBigInt uint64 = 1<<64 - 1
	fmt.Println("Unsigned big int", unsignedBigInt)

	// float
	float1 := 1.1
	var float2 float64 = 1.2
	var float3 float64
	fmt.Println(float1, float2, float3)

	// strings
	var str0 string
	var str1 string = "Hello spec 👍 \n\t"
	var str2 string = `Without spec \n\t`

	fmt.Println(str0, str2, str1)

	// byte (uint8)
	var rawBinary byte = '\x27'

	// rune (uint32) - full UTF-8
	var smile rune = '👍'

	fmt.Println(rawBinary, smile)

	// length of the sting
	byteLen := len(str1)
	runLen := utf8.RuneCountInString(str1)

	fmt.Println(`String length:`, byteLen, runLen)
}
