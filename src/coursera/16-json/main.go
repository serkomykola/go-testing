package main

import (
	"encoding/json"
	"fmt"
)

type User struct {
	ID       int    `json:"user_id,string"`
	Username string `json:",omitempty"`
	phone    string
	Address  string `json:"-"`
}

var jsonStr = `{"user_id": "23", "username": "Serko", "phone": "232314-3412"}`

var jsonStr2 = `[
	{"id": 123, "name": "serko"},
	{"ID": "123", "address": "kiev"}
]`

func main() {
	fmt.Println("JSON")

	data := []byte(jsonStr)
	u := &User{}

	json.Unmarshal(data, u)
	fmt.Printf("result unmarshal: \n\t%#v\n\n", u)

	u.phone = "123"
	result, err := json.Marshal(u)
	if err != nil {
		panic(err)
	}
	fmt.Printf("result marshal: \n\t%s\n\n", string(result))

	// empty intrefaces
	var user1 interface{}
	data2 := []byte(jsonStr2)
	json.Unmarshal(data2, &user1)
	fmt.Printf("result unmarshal empty: \n\t%#v\n\n", user1)

	user2 := map[string]interface{}{
		"ID":   34,
		"name": "nikolas",
	}

	res2, _ := json.Marshal(user2)
	fmt.Printf("result marshal: \n\t%s\n\n", string(res2))
}
