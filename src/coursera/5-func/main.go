package main

import (
	"fmt"
)

func sum(in ...int) (result int) {
	defer fmt.Println("After calculation")

	for _, val := range in {
		result += val
	}

	return
}

func deferTestAndPanic() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("panic there: ", err)
		}
	}()

	fmt.Println("Some message")
	panic("Sommesing bad happend")

}

func main() {
	fmt.Println("test")

	sumIn := []int{3, 4, 5}
	sumRes := sum(sumIn...)

	fmt.Println(sumIn, "Res:", sumRes)

	// anonymus func
	func(in string) {
		fmt.Println("Receive: ", in)
	}("Test str")

	printer := func(str string) {
		fmt.Println("Printer str: ", str)
	}

	// define new data type
	type printerType func(string)
	func(callback printerType) {
		callback("My string")
	}(printer)

	// closure function
	prefixer := func(prefix string) printerType {
		return func(in string) {
			fmt.Printf("Closure with prefix %v, str: %v \n", prefix, in)
		}
	}

	prefixer("success")("log item")

	// panic
	deferTestAndPanic()
}
