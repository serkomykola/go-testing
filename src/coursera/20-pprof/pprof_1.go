package main

import (
	"fmt"
	"net/http"
	_ "net/http/pprof"
	"time"
)

type Post struct {
	ID       int
	Text     string
	Author   string
	Comments int
	time     time.Time
}

func handler(w http.ResponseWriter, r *http.Request) {
	s := ""
	for i := 0; i < 1000; i++ {
		p := &Post{ID: i, Text: fmt.Sprintf("new post %d", i)}

		s += fmt.Sprintf("%#v", p)
	}

	w.Write([]byte(s))
}

func main() {
	fmt.Println("pprof simple")

	http.HandleFunc("/", handler)

	fmt.Println("Starting server at 8081")
	fmt.Println(http.ListenAndServe(":8081", nil))
}
