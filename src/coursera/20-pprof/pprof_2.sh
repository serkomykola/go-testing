# go build -o pprof_2 pprof_2.go && ./pprof_2
# ab -t 300 -n 10000000000 -c 10 http://127.0.0.1:8081/

curl http://127.0.0.1:8081/debug/pprof/trace?seconds=8 -o trace.out

go tool trace -http "127.0.0.1:8081" pprof_2 trace.out