package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	_ "net/http/pprof"
	"runtime"
	"time"
)

type Post struct {
	ID       int
	Text     string
	Author   string
	Comments int
	Time     time.Time
}

func handler(w http.ResponseWriter, r *http.Request) {
	result := ""
	for i := 0; i < 1000; i++ {
		post := &Post{ID: i, Text: fmt.Sprintf("new post %d", i), Time: time.Now()}

		jsonRaw, _ := json.Marshal(post)
		result += string(jsonRaw)
	}

	time.Sleep(3 * time.Millisecond)

	w.Write([]byte(result))
}

func main() {
	fmt.Println("pprof simple")
	runtime.GOMAXPROCS(4)

	http.HandleFunc("/", handler)

	fmt.Println("Starting server at 8081")
	fmt.Println(http.ListenAndServe(":8081", nil))
}
