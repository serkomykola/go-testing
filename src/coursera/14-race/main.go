package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

var totalOperations int32 = 0

func inc() {
	atomic.AddInt32(&totalOperations, 1)
}

func main() {
	fmt.Println("go run -race")

	// Mutex
	var counters = map[int]int{}
	mu := &sync.Mutex{}

	for i := 0; i < 5; i++ {
		go func(counters map[int]int, th int, mu *sync.Mutex) {
			for j := 0; j < 5; j++ {
				mu.Lock()
				counters[th*10+j]++
				mu.Unlock()
			}
		}(counters, i, mu)
	}

	time.Sleep(2 * time.Millisecond)

	mu.Lock()
	fmt.Println("counters result", counters)
	mu.Unlock()

	// Atomic
	for i := 0; i < 10000; i++ {
		go inc()
	}

	time.Sleep(2 * time.Millisecond)
	fmt.Println("result inc", totalOperations)

}
