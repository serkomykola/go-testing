package main

import (
	"fmt"
	"time"
)

func getComments() chan string {
	// need to use buffer channel
	result := make(chan string, 1)
	go func(out chan<- string) {
		time.Sleep(2 * time.Second)
		fmt.Println("Comments received")

		out <- "received 32 comments"
	}(result)

	return result
}

func getPage(req int) {
	fmt.Println("request start", req)

	resultCh := getComments()

	time.Sleep(1 * time.Second)
	fmt.Println("Article text generated")

	commentsData := <-resultCh
	fmt.Println("Comments text generated", commentsData)

	fmt.Println("request finished", req)
}

func main() {
	fmt.Println("Async loading data")

	for i := 0; i < 3; i++ {
		go getPage(i)
	}

	fmt.Scanln()

}
