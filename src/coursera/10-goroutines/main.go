package main

import (
	"fmt"
	"runtime"
	"strings"
)

const (
	iterationsNum = 7
	goroutinesNum = 5
)

func doSomeWork(in int) {
	for j := 0; j < iterationsNum; j++ {
		fmt.Printf(formatWork(in, j))
		runtime.Gosched()
	}
}

func formatWork(in, j int) string {
	return fmt.Sprintln(strings.Repeat("  ", in), "█",
		strings.Repeat("  ", goroutinesNum-in),
		"th", in,
		"iter", j, strings.Repeat("■", j))
}

func main() {
	for i := 0; i < goroutinesNum; i++ {
		// go doSomeWork(i)
	}

	// chanels base
	ch1 := make(chan int)
	go func(in chan int) {
		val := <-in
		fmt.Println("Go: Get from chan", val)
		fmt.Println("Go: After read from chan")
	}(ch1)

	ch1 <- 42

	fmt.Println("Main: After put to chan")

	// chanels loop
	ch2 := make(chan int)

	go func(out chan<- int) {
		for i := 0; i < 4; i++ {
			fmt.Println("Before", i)
			out <- i
			fmt.Println("After", i)
		}
		close(out) // завершую цикл range
		fmt.Println("Generator finish")
	}(ch2)

	// chanel range
	for i := range ch2 {
		fmt.Println("Receive", i)
	}

	// select
	sch1 := make(chan int)
	sch2 := make(chan int)

	select {
	case val := <-sch1:
		fmt.Println("ch1 val", val)
	case sch2 <- 2:
		fmt.Println("put val to ch2")
	default:
		fmt.Println("Default case")
	}

	// cancel chanel
	cancelCh := make(chan struct{})
	dataCh := make(chan int)

	go func(cancelCh chan struct{}, dataCh chan int) {
		val := 0
		for {
			select {
			case <-cancelCh:
				return
			case dataCh <- val:
				val++
			}
		}
	}(cancelCh, dataCh)

	for val := range dataCh {
		fmt.Println("Received data val", val)
		if val > 3 {
			fmt.Println("Send cancel")
			cancelCh <- struct{}{}
			break
		}
	}
}
