package main

import (
	"bufio"
	"bytes"
	"strings"
	"testing"
)

var testOk = `1
2
3
4
4
4
5
6`

var testOkResult = `1
2
3
4
5
6
`

func TestOk(t *testing.T) {
	in := bufio.NewReader(strings.NewReader(testOk))
	out := new(bytes.Buffer)

	err := uniq(in, out)

	if err != nil {
		t.Errorf("Test for Ok failed - failed run")
	}

	result := out.String()
	if result != testOkResult {
		t.Errorf("Test for Ok failed - result not match: \n %v, %v \n", testOkResult, result)
	}
}

var testFail = `1
2
3
4
3`

func TestForError(t *testing.T) {
	in := bufio.NewReader(strings.NewReader(testFail))
	out := new(bytes.Buffer)

	err := uniq(in, out)

	if err == nil {
		t.Errorf("Test for Failed: %v", err)
	}

}
