package main

import (
	"fmt"
)

func main() {
	var a1 [3]int
	fmt.Println(`array`, a1)
	fmt.Printf("short %v \n", a1)
	fmt.Printf("full %#v \n", a1)

	a2 := [...]int{2, 3, 4, 5}
	fmt.Printf("full %#v \n", a2)

	// slices
	var buf0 []int
	buf1 := []int{}
	buf2 := []int{42}
	buf3 := make([]int, 0)
	buf4 := make([]int, 5)
	buf5 := make([]int, 5, 10)

	fmt.Println(buf0, buf1, buf2, buf3, buf4)
	fmt.Println("Length and capacity of the slice", buf5, len(buf5), cap(buf5))

	// append
	buf6 := make([]int, 3)
	buf6 = append(buf6, 99)
	buf6 = append(buf6, buf2...)
	fmt.Println("append", buf6, len(buf6), cap(buf6))

	// slice
	buf7 := []int{1, 2, 3, 4, 5, 6, 7}
	sl1 := buf7[:2]
	sl2 := buf7[2:]
	sl3 := buf7[:]
	sl2[0] = 99
	fmt.Println("slice", buf7, sl1, sl2, sl3)

	// copy slices
	copyBuf := make([]int, len(buf7), cap(buf7))
	copy(copyBuf, buf7)

	copyInts := []int{1, 2, 3, 4}
	copy(copyInts[1:3], []int{9, 8})

	fmt.Println("copy slices", copyBuf, copyInts)

	// map
	var user map[string]string = map[string]string{
		"name": "data",
		"val":  "value",
	}

	fmt.Println("map", user)

	mName, exists := user["middlename"]

	fmt.Println("map value", mName, "exists:", exists)

}
