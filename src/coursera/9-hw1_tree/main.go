package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
)

type SortFiles []os.FileInfo

func (a SortFiles) Len() int      { return len(a) }
func (a SortFiles) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a SortFiles) Less(i, j int) bool {
	return a[i].Name() < a[j].Name()
}

func formattingFileinfo(fInfo os.FileInfo) (res string) {
	size := fInfo.Size()
	if fInfo.IsDir() {
		res = fInfo.Name()
	} else if size > 0 {
		res = fmt.Sprintf("%s (%db)", fInfo.Name(), size)
	} else {
		res = fmt.Sprintf("%s (empty)", fInfo.Name())
	}

	return
}

func dirSubTree(out io.Writer, path string, printFiles bool, prefix string) error {
	// start reading file
	file, err := os.Open(path) // For read access.
	if err != nil {
		return err
	}

	// read all fiels in the path
	files, err := file.Readdir(1000)
	if err != nil {
		return err
	}

	// sorting files by fileName
	sort.Sort(SortFiles(files))

	// searching last idx in the list
	var lastIdx int
	for idx, fInfo := range files {
		// check directory
		if fInfo.IsDir() {
			lastIdx = idx
		}

		// check file
		if printFiles && fInfo.Mode().IsRegular() {
			lastIdx = idx
		}
	}

	var errResult error
	for idx, fInfo := range files {
		// define the separating char
		var char string
		var subPrefix string
		if idx == lastIdx {
			char = "└"
			subPrefix = "	"
		} else {
			char = "├"
			subPrefix = "│	"
		}
		// generate full output string
		// outLine := fmt.Sprintf("%s%s", prefix, char)
		outLine := prefix + char + "───" + formattingFileinfo(fInfo) + "\n"

		// check directory
		if fInfo.IsDir() {
			fmt.Fprintf(out, outLine)

			// generate sub-path
			subPath := filepath.Join(path, fInfo.Name())
			// read sub-path recursive
			errResult = dirSubTree(
				out,
				subPath,
				printFiles,
				prefix+subPrefix,
			)

			if errResult != nil {
				break
			}
		}

		// check file
		if printFiles && fInfo.Mode().IsRegular() {
			fmt.Fprintf(out, outLine)
		}
	}

	return errResult
}

func dirTree(out io.Writer, path string, printFiles bool) error {
	return dirSubTree(out, path, printFiles, "")
}

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}
