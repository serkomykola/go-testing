package main

import (
	"context"
	"fmt"
	"strconv"
	"time"
)

type executeFunc func(<-chan string, chan<- string)

// SingleHash using for
func SingleHash(in <-chan string, out chan<- string) {
	fmt.Println("SingleHash waiting")

	data := <-in
	fmt.Println("func 1: ", data)

	//resourceChan := make(chan struct{}, 1)
	inCrcChan := make(chan string, 2)
	outCrcChan := make(chan string, 2)

	// go md5Func(data, inCrcChan, resourceChan)
	go crc32Func(inCrcChan, outCrcChan)

	// pass resources
	inCrcChan <- data
	inCrcChan <- DataSignerMd5(data)

	// waiting response
	go func(in <-chan string, out chan<- string) {
		str1 := <-in

		fmt.Println("str1", str1)
		str2 := <-in

		fmt.Println("str12", str2)

		out <- str1 + "~" + str2
	}(outCrcChan, out)
}

// MultiHash using for
func MultiHash(in <-chan string, out chan<- string) {
	fmt.Println("MultiHash waiting")
	data := <-in
	fmt.Println("func 2: ", data)

	for i := 0; i < 6; i++ {
		out <- data + "~2" + strconv.Itoa(i)
		time.Sleep(100 * time.Millisecond)
	}

}

// CombineResult for collocation
func CombineResults(in <-chan string, out chan<- string) {
	th0 := <-in
	th1 := <-in
	th2 := <-in
	th3 := <-in
	th4 := <-in
	th5 := <-in

	out <- th0 + "-" + th1 + "-" + th2 + "-" + th3 + "-" + th4 + "-" + th5
}

// Final func for close chanels
func Final(in <-chan string, out chan<- string) {
	fmt.Println("Final")

	res := <-in
	printRes(res)
}

func printRes(str string) {
	fmt.Println("Final result", str)
}

func crc32Func(in <-chan string, out chan<- string) {
	str := <-in
	fmt.Println("crc32", str)
	res := DataSignerCrc32(str)

	fmt.Println("crc32 func", res)
	out <- res
}

func md5Func(str string, out chan<- string, resourceChan chan struct{}) {
	resourceChan <- struct{}{}

	fmt.Println("md5", str)
	res := DataSignerMd5(str)
	fmt.Println("md5 func", res)

	out <- res
	<-resourceChan
}

// ExecutePipeline using
func ExecutePipeline(ctx context.Context, data string, pipe <-chan executeFunc) {
	inChan := make(chan string, 10)
	outChan := make(chan string, 10)

	go func(ctx context.Context, in <-chan string, out chan<- string) {
		for {
			select {
			case res := <-outChan:
				fmt.Println("Select ", res)
				inChan <- res
			case <-ctx.Done():
				close(inChan)
				close(outChan)
				return
			}
		}
	}(ctx, inChan, outChan)

	go func(pipe <-chan executeFunc, in <-chan string, out chan<- string) {
		for newFunc := range pipe {
			newFunc(inChan, outChan)
		}
	}(pipe, inChan, outChan)

	// put data to input chan
	inChan <- data

	fmt.Println("After select")
}

func main() {
	fmt.Println("Home work 2")

	var data string = "0"
	pipeLine := make(chan executeFunc, 1)
	ctx, finish := context.WithCancel(context.Background())

	go ExecutePipeline(ctx, data, pipeLine)

	// add func to the pipeline
	pipeLine <- SingleHash
	pipeLine <- MultiHash
	pipeLine <- CombineResults
	pipeLine <- Final

	time.Sleep(5 * time.Second)
	finish()
}
