package main

import (
	"fmt"
	"html/template"
	"net/http"
)

type User struct {
	ID     int
	Name   string
	Active bool
}

func (u *User) PrintActive() string {
	if u.Active {
		return "method says that " + u.Name + " is active"
	}

	return ""
}

var tmplMethod *template.Template
var tmplUsers *template.Template
var err error

func init() {
	tmplFunc := template.FuncMap{
		"OddUser": IsUserOdd,
	}

	tmplMethod, err = template.
		New("").
		Funcs(tmplFunc).
		ParseFiles("method.html")
	tmplUsers = template.Must(template.ParseFiles("users.html"))

	if err != nil {
		panic(err)
	}
}

func IsUserOdd(u *User) bool {
	return u.ID%2 != 0
}

func handlerUsers(w http.ResponseWriter, r *http.Request) {
	users := []User{
		User{12, "Serko", true},
		User{23, "<i>Mykola</i>", false},
		User{45, "Serko123", true},
	}

	tmplUsers.Execute(w,
		struct {
			Users []User
		}{
			users,
		})
}

func handlerMethod(w http.ResponseWriter, r *http.Request) {
	users := []User{
		User{12, "Serko2", true},
		User{23, "<i>Mykola2</i>", false},
		User{45, "Serko123", true},
	}

	err := tmplMethod.ExecuteTemplate(
		w,
		"method.html",
		struct {
			Users []User
		}{
			users,
		})

	if err != nil {
		panic(err)
	}
}

func main() {
	fmt.Println("http html template")

	http.HandleFunc("/method", handlerMethod)
	http.HandleFunc("/", handlerUsers)

	fmt.Println("Server starting at 8080")
	http.ListenAndServe(":8080", nil)
}
