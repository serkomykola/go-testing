package main

import (
	"fmt"
	"net/http"
	"text/template"
)

type tplParams struct {
	URL     string
	Browser string
}

const EXAMPLE = `
Browser {{.Browser}}

Your url is {{.URL}}
`

func handler(w http.ResponseWriter, r *http.Request) {
	tpl := template.New(`example`)
	tpl, _ = tpl.Parse(EXAMPLE)

	params := tplParams{
		URL:     r.URL.String(),
		Browser: r.UserAgent(),
	}

	tpl.Execute(w, params)
}

func main() {
	fmt.Println("http templates")

	http.HandleFunc("/", handler)

	fmt.Println("Starting server at 8080")
	http.ListenAndServe(":8080", nil)
}
