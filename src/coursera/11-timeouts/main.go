package main

import (
	"fmt"
	"time"
)

func longSqlQuery() chan struct{} {
	ch := make(chan struct{})

	go func(ch chan struct{}) {
		time.Sleep(3 * time.Microsecond)
		ch <- struct{}{}
	}(ch)

	return ch
}

func sayHello() {
	fmt.Println("Say hello")
}

func main() {
	fmt.Println("Go timeouts")

	timer := time.NewTimer(1 * time.Second)

	select {
	case <-timer.C:
		fmt.Println("timer.C happened")
	case <-time.After(time.Minute):
		// not free resources for not happened after
		fmt.Println("time.After happened")
	case result := <-longSqlQuery():
		fmt.Println("Execution seccessfull", result)
		// free resources
		if !timer.Stop() {
			<-timer.C
		}
	}

	// interval
	ticker := time.NewTicker(1 * time.Second)
	i := 0
	for tik := range ticker.C {
		i++
		fmt.Println("Tike i:", i, "time", tik)

		// break loop
		if i > 5 {
			//stop ticker
			ticker.Stop()
			break
		}
	}

	fmt.Println("total ", i)

	// afterfunc
	afterTimer := time.AfterFunc(1*time.Second, sayHello)

	fmt.Scanln()
	afterTimer.Stop()

}
