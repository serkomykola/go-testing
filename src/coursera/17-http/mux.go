package main

import (
	"fmt"
	"net/http"
	"time"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello Mux. Url: %s \n", r.URL.String())
}

func main() {
	fmt.Println("Http mux")

	mux := http.NewServeMux()
	mux.HandleFunc("/", handler)

	server := http.Server{
		Addr:         ":8080",
		Handler:      mux,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}

	fmt.Println("Server at :8080")
	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}
