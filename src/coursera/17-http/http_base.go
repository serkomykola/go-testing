package main

import (
	"fmt"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello, serko!")
	w.Write([]byte("|||"))
}

func main() {
	fmt.Println("http start")

	http.HandleFunc("/page", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "Single page "+r.URL.String())
	})

	http.HandleFunc("/pages/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "Multiple pages "+r.URL.String())
	})

	http.HandleFunc("/", handler)
	fmt.Println("Serve at :8080")
	http.ListenAndServe(":8080", nil)
}
