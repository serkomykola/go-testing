package main

import (
	"fmt"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Method", r.Method)

	myParam := r.URL.Query().Get("param")
	if myParam != "" {
		fmt.Fprintln(w, "My params is", myParam)
	}

	key := r.FormValue("key")
	if key != "" {
		fmt.Fprintln(w, "Key is", key)
	}
}

func main() {
	fmt.Println("HTTP params")

	http.HandleFunc("/", handler)

	fmt.Println("Starting server at :8080")
	http.ListenAndServe(":8080", nil)
}
