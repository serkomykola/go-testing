package main

import (
	"fmt"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	w.Write([]byte(`
		Hello world <br />
		<img src="/static/nitish.jpg" width="300">
	`))
}

func main() {
	fmt.Println("static files")

	http.HandleFunc("/", handler)

	// remove static from the path
	// img/ is base of images
	staticHandler := http.StripPrefix(
		"/static/",
		http.FileServer(http.Dir("./img")),
	)

	http.Handle("/static/", staticHandler)

	fmt.Println("Serve at :8080")
	http.ListenAndServe(":8080", nil)
}
