package main

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

type Params struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

var uploadFormTpl = []byte(`
<html>
<body>
<form action="/upload" method="post" enctype="multipart/form-data">
	Image: <input type="file" name="my_file">
	<button type="submit">Upload</button>
</form
</body>
</html>
`)

func uploadPage(w http.ResponseWriter, r *http.Request) {
	// parse only 5MB. More will be saved in temporary files
	r.ParseMultipartForm(5 * 1024 * 1025)
	file, handler, err := r.FormFile("my_file")

	if err != nil {
		fmt.Println(err)
		return
	}

	defer file.Close()

	fmt.Fprintf(w, "Filename: %#v \n", handler.Filename)
	fmt.Fprintf(w, "Header: %#v \n", handler.Header)

	// calculate md5 sum for this file
	hasher := md5.New()
	io.Copy(hasher, file)

	fmt.Fprintf(w, "md5 %x \n", hasher.Sum(nil))
}

// example for json
// curl -X POST "http://localhost:8080/upload-json" -H "accept: application/json" -H "Content-Type: application/json" -d '{"name":"serko","age":28,"level":""}'
func uploadRawBody(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	p := &Params{}
	err = json.Unmarshal(body, p)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	fmt.Fprintf(w, "Body %#v \n", string(body))
	fmt.Fprintf(w, "Content-Type: %v \n", r.Header.Get("Content-Type"))
	fmt.Fprintf(w, "Content: %#v \n", p)
	response, _ := json.Marshal(p)
	fmt.Fprintf(w, "Content Marshal: %s \n", response)
}

func main() {
	fmt.Println("File upload")

	http.HandleFunc("/upload", uploadPage)
	http.HandleFunc("/upload-json", uploadRawBody)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write(uploadFormTpl)
	})

	fmt.Println("Serve at :8080")
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		panic(err)
	}
}
