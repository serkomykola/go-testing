package main

import (
	"fmt"
	"net/http"
)

type Handler struct {
	Name string
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello %s. Url: %s \n", h.Name, r.URL.String())
}

func main() {
	fmt.Println("Http advance")

	handler := &Handler{Name: "Serko"}
	http.Handle("/test/", handler)

	rootHandler := &Handler{Name: "root"}
	http.Handle("/", rootHandler)

	fmt.Println("Server at :8080")
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		panic(err)
	}
}
