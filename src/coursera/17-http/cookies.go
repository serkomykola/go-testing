package main

import (
	"fmt"
	"net/http"
	"time"
)

const SESSION_COOKIE = "session"

var loginForm = `
<html>
<head>
<title>Login Form</title>
</head>
<body>
<h3>Login form</h3>
<form method="POST" action="/login">
	Логін
	<input type="text" name="login" />

	Пароль
	<input type="password" name="pass" />

	<button type="submit">Send</button>
</form>
</body>
</html>
`

func login(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.Write([]byte(loginForm))
		return
	}

	name := r.FormValue("login")
	expiration := time.Now().Add(10 * time.Second)

	cookie := http.Cookie{
		Name:    SESSION_COOKIE,
		Value:   name,
		Expires: expiration,
	}

	http.SetCookie(w, &cookie)
	http.Redirect(w, r, "/", http.StatusFound)
}

func logout(w http.ResponseWriter, r *http.Request) {
	session, err := r.Cookie(SESSION_COOKIE)
	if err != http.ErrNoCookie {
		session.Expires = time.Now().AddDate(0, 0, -1)
		http.SetCookie(w, session)
	}

	http.Redirect(w, r, "/", http.StatusFound)
}

func mainPage(w http.ResponseWriter, r *http.Request) {
	session, err := r.Cookie(SESSION_COOKIE)
	logged := err != http.ErrNoCookie

	w.Header().Set("ResponseID", "wertert")
	// fmt.Fprintln(w, "Headers"+r.UserAgent()+r.Header.Get("Accept"))

	if logged {
		fmt.Fprintln(w, `<a href="/logout">logout</a>`)
		fmt.Fprintln(w, "Welcome "+session.Value)
	} else {
		fmt.Fprintln(w, `<a href="/login">login</a>`)
		fmt.Fprintln(w, "You need to login")
	}
}

func main() {
	fmt.Println("HTTP cookies")

	http.HandleFunc("/login", login)
	http.HandleFunc("/logout", logout)
	http.HandleFunc("/", mainPage)

	fmt.Println("Serve at :8080")
	http.ListenAndServe(":8080", nil)
}
