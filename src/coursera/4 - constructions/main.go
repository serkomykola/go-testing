package main

import (
	"fmt"
)

func main() {
	fmt.Println("Structures")

	// simple
	boolVal := true
	if boolVal {
		fmt.Println("it is simple construction")
	}

	// with additional (init) block
	mapVal := map[string]string{
		"name": "Serko",
	}

	if _, keyExists := mapVal["name"]; keyExists {
		fmt.Println("Key exists")
	}

	// switch
	intVal := 3
	switch {
	case intVal > 0 && intVal < 100:
		fmt.Println("Below 10, obove 0")
	default:
		fmt.Println("Default case")
	}

	// loop
	for {
		fmt.Println("Infinity loop")
		break
	}

	isTrue := true
	for isTrue {
		fmt.Println("Example 2")
		isTrue = false
	}

	slice := []int{1, 2, 3, 4, 56}

	for idx, val := range slice {
		fmt.Println("loop range slice", idx, val)
	}

	str := "Привіт, Світ!"
	for pos, char := range str {
		fmt.Printf("%#U at pos %d\n", char, pos)
	}
}
