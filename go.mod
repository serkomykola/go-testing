module testing

go 1.13

require (
	github.com/asaskevich/govalidator v0.0.0-20200108200545-475eaeb16496
	github.com/buaazp/fasthttprouter v0.1.1
	github.com/corpix/uarand v0.1.1 // indirect
	github.com/gin-gonic/gin v1.5.0
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible // indirect
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/schema v1.1.0
	github.com/gorilla/websocket v1.4.0
	github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428
	github.com/julienschmidt/httprouter v1.2.0
	github.com/mailru/easyjson v0.7.1
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pelletier/go-toml v1.6.0 // indirect
	github.com/pkg/errors v0.8.0
	github.com/rvasily/examplerepo v0.0.0-20170218071937-9b59d66a1aa9
	github.com/shiyanhui/hero v0.0.2
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.6.2
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	github.com/valyala/fasthttp v1.9.0
	github.com/yggdrasil-network/yggdrasil-go v0.3.12 // indirect
	go.uber.org/zap v1.10.0
	golang.org/x/sys v0.0.0-20200202164722-d101bd2416d5 // indirect
	google.golang.org/grpc v1.21.0
	gopkg.in/ini.v1 v1.52.0 // indirect
	gopkg.in/telegram-bot-api.v4 v4.6.4
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
